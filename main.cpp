#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <thread>
#include <unistd.h>

#define BTN01 26
#define LED01 12
#define LED02 19
#define LED03 18
#define LED04 13

using namespace std;

pthread_t tmp_thread_led_01, tmp_thread_led_02, tmp_thread_led_03, tmp_thread_led_04; // for temporary thread which will be store thread id of second thread 
int end_led = 0;

int k;

void myInterrupt(){
	if( digitalRead(BTN01) == 0 ){     //button pressed?
		delay(80);     //contact vibration time
		if( digitalRead(BTN01) == 0 ){     //button still pressed?
			puts("Pressed!");
			puts("Changing light direction.");
			switch(k){
				case(1): k=2; delay(1000); break;
				case(2): k=1; delay(1000); break;
			}
			digitalWrite(LED01, LOW);
			pthread_cancel(tmp_thread_led_01); // for cancel thread_one 
			digitalWrite(LED02, LOW);
			pthread_cancel(tmp_thread_led_02); // for cancel thread_two 
			digitalWrite(LED03, LOW);
			pthread_cancel(tmp_thread_led_03); // for cancel thread_three 
			digitalWrite(LED04, LOW);
			pthread_cancel(tmp_thread_led_04); // for cancel thread_four
			delay(1000);
		}
	}
}//myInterrupt



void* led_01(void* p){	
	tmp_thread_led_01 = pthread_self(); 	// store thread_two id to tmp_thread_led_01

	wiringPiSetup();
	int led = 12;
	printf("Lets LED %d...\n", led);
	digitalWrite(led, HIGH);

	while(true){
		digitalWrite(led, HIGH);
		delay(500);
		digitalWrite(led, LOW);
		delay(500);
	}//while
	end_led = 1;
	pthread_exit(NULL); // for exit from thread
}//led_01



void* led_02(void* p){	
	tmp_thread_led_02 = pthread_self(); 	// store thread_two id to tmp_thread_led_02

	wiringPiSetup();
	int led = 19;
	printf("Lets LED %d...\n", led);
	digitalWrite(led, HIGH);

	while(true){
		digitalWrite(led, HIGH);
		delay(500);
		digitalWrite(led, LOW);
		delay(500);
	}//while
	end_led = 1;
	pthread_exit(NULL); // for exit from thread
}//led_02



void* led_03(void* p){
	tmp_thread_led_03 = pthread_self(); 	// store thread_three id to tmp_thread_led_03

	wiringPiSetup();
	int led = 18;
	printf("Lets LED %d...\n", led);
	digitalWrite(led, HIGH);

	while(true){
		digitalWrite(led, HIGH);
		delay(500);
		digitalWrite(led, LOW);
		delay(500);
	}//while
	end_led = 1;
	pthread_exit(NULL); // for exit from thread
}//led_03



void* led_04(void* p){
	tmp_thread_led_04 = pthread_self(); 	// store thread_four id to tmp_thread_led_04

	wiringPiSetup();
	int led = 13;
	printf("Lets LED %d...\n", led);
	digitalWrite(led, HIGH);

	while(true){
		digitalWrite(led, HIGH);
		delay(500);
		digitalWrite(led, LOW);
		delay(500);
	}//while
	end_led = 1;
	pthread_exit(NULL); // for exit from thread
}//led_04



//////////////////////////////////////////////////////////////////////////



int main(int argc, char** argv){
	pthread_t thread_one, thread_two, thread_three, thread_four;	// declare threads
	k=1;

	wiringPiSetupGpio();
    pinMode(BTN01, INPUT);
    pullUpDnControl (BTN01, PUD_UP);
    wiringPiISR(BTN01, INT_EDGE_FALLING, &myInterrupt);

	while(1){
		if(k==1){
			puts("Start LED light direction <----");
			delay(100);
			pthread_create(&thread_one, NULL, led_01, NULL);	// create thread_two 
			delay(100);
			pthread_create(&thread_two, NULL, led_02, NULL);	// create thread_three
			delay(100);
			pthread_create(&thread_three, NULL, led_03, NULL);	// create thread_four
			delay(100);
			pthread_create(&thread_four, NULL, led_04, NULL);	// create thread_four

			pthread_join(thread_one, NULL); // waiting for when thread_one is completed 
			pthread_join(thread_two, NULL); // waiting for when thread_two is completed
			pthread_join(thread_three, NULL); // waiting for when thread_three is completed
			pthread_join(thread_four, NULL); // waiting for when thread_four is completed

		}else if(k==2){
			puts("Start LED light direction ---->");
			delay(100);
			pthread_create(&thread_one, NULL, led_04, NULL); 
			delay(100);
			pthread_create(&thread_two, NULL, led_03, NULL);
			delay(100);
			pthread_create(&thread_three, NULL, led_02, NULL);
			delay(100);
			pthread_create(&thread_four, NULL, led_01, NULL);

			pthread_join(thread_one, NULL); // waiting for when thread_one is completed 
			pthread_join(thread_two, NULL); // waiting for when thread_two is completed
			pthread_join(thread_three, NULL); // waiting for when thread_three is completed
			pthread_join(thread_four, NULL); // waiting for when thread_four is completed

		}
	}//while

	return 0;
}
